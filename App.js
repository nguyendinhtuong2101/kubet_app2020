import React, { Component } from 'react';
import {
  AppRegistry, ImageBackground, StyleSheet, Text, View,
    SafeAreaView,ScrollView,Image,TouchableOpacity ,Alert,
    ToastAndroid,Button,ProgressBarAndroid,
Linking,
  PermissionsAndroid
} from 'react-native';
import RNFetchBlob from 'rn-fetch-blob';

export async function request_storage_runtime_permission() {

  try {
    const granted = await PermissionsAndroid.request(
      PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
      {
        'title': 'ReactNativeCode Storage Permission',
        'message': 'ReactNativeCode App needs access to your storage to download Photos.'
      }
    )
    if (granted === PermissionsAndroid.RESULTS.GRANTED) {

    //  Alert.alert("Storage Permission Granted.");
    }
    else {

    //  Alert.alert("Storage Permission Not Granted");

    }
  } catch (err) {
    console.warn(err)
  }
}
export default class Myproject extends Component {
  async componentDidMount() {

    await request_storage_runtime_permission()

  }

  constructor(props) {
      super(props);
      this.state = {
        progress: 0,
        loading: false,
        links: [
            { title: 'facebook', linkfb: 'https://www.facebook.com/iwin68clubvn' },
            { title: 'youtube', linkfb: 'https://www.youtube.com/channel/UCDrtzZiyAioAQRpN8DxkRDg' },
            { title: 'twitter', linkfb: 'https://twitter.com/iwin68' },
            { title: 'support', linkfb: 'https://core.vchat.vn/service/chat?code=2154&jwt=50a941b33af9834db2e56f2aea64511f' },
          ],
      };
    }
handleButtonPress(item) {
    const { title, linkdk } = item
    this.props.navigation.navigate('ĐĂNG KÝ - ĐĂNG NHẬP', { title, linkdk })
  }
  downloadImage = () => {
    this.setState({
      progress: 0,
      loading: true
    });
    var date = new Date();
    var image_URL = 'https://iwin68.info/wp-content/uploads/c5uilfo6r6e93.apk';
    var ext = this.getExtention(image_URL);
    ext = "." + ext[0];
    const { config, fs } = RNFetchBlob;
    let PictureDir = fs.dirs.PictureDir
    let options = {
      fileCache: true,
      addAndroidDownloads: {
        useDownloadManager: true,
        notification: true,
        path: PictureDir + "/image_" + Math.floor(date.getTime()
          + date.getSeconds() / 2) + ext,
        description: 'Image'
      }
    }
    config(options).fetch('GET', image_URL).then((res) => {
        this.setState({
          progress: 100,
          loading: false
        });
        ToastAndroid.showWithGravity(
          "Tải thành công!",
          ToastAndroid.SHORT,
          ToastAndroid.BOTTOM
        );
    });
  }

  getExtention = (filename) => {
    return (/[.]/.exec(filename)) ? /[^.]+$/.exec(filename) :
      undefined;
  }


    actualDownload = () => {
       this.setState({
         progress: 0,
         loading: true
       });
       let dirs = RNFetchBlob.fs.dirs;
       RNFetchBlob.config({
         // add this option that makes response data to be stored as a file,
         // this is much more performant.
         path: dirs.DownloadDir + "/path-to-file.png",
         fileCache: true
       })
         .fetch(
           "GET",
           "https://iwin68.info/wp-content/uploads/c5uilfo6r6e93.apk",
           {
             //some headers ..
           }
         )
         .progress((received, total) => {
           console.log("progress", received / total);
           this.setState({ progress: received / total });
         })
         .then(res => {
           this.setState({
             progress: 100,
             loading: false
           });
           ToastAndroid.showWithGravity(
             "Tải thành công!",
             ToastAndroid.SHORT,
             ToastAndroid.BOTTOM
           );
         });
     };



  getExtention = (filename) => {
      return (/[.]/.exec(filename)) ? /[^.]+$/.exec(filename) :
        undefined;
    }


  render() {
    const link ="http://13.212.177.19/asset/image/";
    let bg   = link+"bg.png";
    let logo = link+"logo.png";
    let chat = link+"chat.png";
    let logobaner = link+"logobaner.png";
    let text_1    = link+"text_1.png";
    let sloan     = link+"sloan.png";
    let game     = link+"game.png";
    let text_2    = link+"text_2.png";
    let download     = link+"download.png";
    let text_3    = link+"text_3.png";
    let text_4    = link+"text_4.png";
    let text_5    = link+"text_5.png";
    let text_6    = link+"text_6.png";
    let fb        = link+"fb.png";
    let yt        = link+"yt.png";
    let tt        = link+"tt.png";

    let link_fb ="https://www.facebook.com/iwin68clubvn";
    let link_yt ="https://www.youtube.com/channel/UCDrtzZiyAioAQRpN8DxkRDg";
    let link_tw ="https://twitter.com/iwin68";
    let link_support ="https://core.vchat.vn/service/chat?code=2154&jwt=50a941b33af9834db2e56f2aea64511f";

    return (
            <View style={styles.container}>
              <ImageBackground source={{uri: bg}} style={styles.image}>
              <View style={styles.top}>
                  <View>
                      <Image style={{width: 60, height: 60}}
                        source={{uri: logo}}
                      />
                  </View>
                  <View>
                  <TouchableOpacity
                   onPress={ ()=>{ Linking.openURL(link_support)}}
                  style={styles.buttonGPlusStyle} activeOpacity={0.5}>
                  <Image style={{width: 60, height: 60}}
                    source={{ uri:chat}}      />
                  </TouchableOpacity>
                  </View>
              </View>

          <View style={{flex: 1, flexDirection: 'column', marginTop:-650 }}>

               <View style={{flex: 5,marginLeft:15}}>
               <Image resizeMode="contain" style={{width: '90%', height: '95%'}}
                 source={{uri: logobaner}}
               />
               </View>
               <View style={{flex: 3 ,alignItems:'center',marginTop:-40 }}>
                  <Image resizeMode="contain" style={{width: '98%',
                  height: '80%'}}
                    source={{uri: sloan}}
                  />
               </View>
               <View style={{flex: 1,alignItems:'center' }}>
                    <Image resizeMode="contain" style={{width: '90%',
                         height: '61%' ,alignItems:'center' ,marginTop:-25,backgroundColor:'orange'}}
                         source={{uri: text_1}}
                         />
               </View>
               <View style={{flex:6,alignItems:'center',marginTop:-30 }}>
                  <Image style={{width: '95%',height: '100%'}}
                    source={{uri: game}}
                  />
               </View>
               <View style={{flex: 2,alignItems:'center'}}>
                  <Image style={{width: '90%',
                  height: '35%'}}
                    source={{uri: text_2}}
                  />
               </View>
               <View style={{flex: 2,alignItems:'center',marginTop:-40  }}>
                 <TouchableOpacity onPress={this.downloadImage}
                 style={styles.buttonGPlusStyle} activeOpacity={0.5}>
                 <Image   style={{width: '80%',
                 height: '90%', }}  source={{ uri:download}}      />
                 </TouchableOpacity>
                 {this.state.loading ? (
                    <ProgressBarAndroid
                      styleAttr="Large"
                      indeterminate={false}
                      progress={this.state.progress}
                    />
                  ) : null}
               </View>
               <View style={{flex: 2,alignItems:'center', marginTop:15 }}>
                  <Image resizeMode="contain" style={{width: '80%',
                  height: '35%'}}
                    source={{uri: text_3}}
                  />
               </View>
               <View resizeMode="contain" style={{flex: 4,alignItems:'center',marginTop:-50 }}>
                  <Image resizeMode="contain" style={{width: '95%',
                  height: '80%'}}
                    source={{uri: text_4}}
                  />
               </View>
               <View style={{flex: 3,alignItems:'center',marginTop:-30  }}>
                  <Image style={{width: '90%',
                  height: '40%'}}
                    source={{uri: text_5}}
                  />
               </View>
               <View style={{flex: 2,alignItems:'center',marginTop:-55  }}>
                  <Image resizeMode="contain" style={{width: '90%',
                  height: '35%'}}
                    source={{uri: text_6}}
                  />
               </View>
               <View style={{flex: 2,alignItems:'center', flexDirection: 'row'}}>
               <View style={{flex: 2,alignItems:'center',marginTop:-55  }}>
                 <TouchableOpacity
                  onPress={ ()=>{ Linking.openURL(link_fb)}}
                 style={styles.buttonGPlusStyle} activeOpacity={0.5}>
                 <Image resizeMode="contain"  style={{width: '90%',
                 height: '90%', }}  source={{ uri:fb}}      />
                 </TouchableOpacity>
               </View>


               <View style={{flex: 2,alignItems:'center',marginTop:-55  }}>
                 <TouchableOpacity
                  onPress={ ()=>{ Linking.openURL(link_yt)}}
                 style={styles.buttonGPlusStyle} activeOpacity={0.5}>
                 <Image resizeMode="contain"  style={{width: '90%',
                 height: '90%', }}  source={{ uri:yt}}      />
                 </TouchableOpacity>
               </View>

               <View style={{flex: 2,alignItems:'center',marginTop:-55  }}>
                   <TouchableOpacity
                    onPress={ ()=>{ Linking.openURL(link_tw)}}
                   style={styles.buttonGPlusStyle} activeOpacity={0.5}>
                   <Image resizeMode="contain"  style={{width: '90%',
                   height: '90%', }}  source={{ uri:tt}}      />
                   </TouchableOpacity>
               </View>

              </View>
              </View>
              </ImageBackground>
            </View>

    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    //flexDirection: "row"
  },
  image: {
    flex: 1,
    resizeMode: "cover",
    justifyContent: "center"
  },
  top:{
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    margin:10,
  },
  contain: {
    flex: 1,
    flexDirection: 'column',
    //alignItems:'center',
    //justifyContent: 'space-between',
    marginTop:-650,
    marginLeft:40
  },
  buttonGPlusStyle: {
    flexDirection: 'row',
    alignItems: 'center',
    //backgroundColor: '#dc4e41',
    //borderWidth: 0.5,
    borderColor: '#fff',
    height: 60,
    borderRadius: 5,
    margin: 5,
  },
});


AppRegistry.registerComponent('Myproject', () => Myproject);
