import React, { Component } from 'react'
import { WebView } from 'react-native-webview'
import { ActivityIndicator, StyleSheet } from 'react-native'

class Facebook extends Component {
  LoadingIndicatorView() {
   return <ActivityIndicator color='#009b88' size='large' style={styles.ActivityIndicatorStyle} />
 }
  render() {

    const  params  = "https://www.facebook.com/iwin68clubvn";
    return  (
          <WebView
          source={{ uri: params }}
          renderLoading={this.LoadingIndicatorView}
          startInLoadingState={true}
          />
        )
  }
}

const styles = StyleSheet.create({
  ActivityIndicatorStyle: {
    flex: 1,
    justifyContent: 'center',
  }
})
export default Facebook
